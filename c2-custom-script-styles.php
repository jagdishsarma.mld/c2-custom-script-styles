<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://jagdish.info/
 * @since             1.0.0
 * @package           C2_Custom_Script_Styles
 *
 * @wordpress-plugin
 * Plugin Name:       C2 Custom Script and Styles
 * Plugin URI:        https://c2code.jagdish.info/
 * Description:       This plugin provide option to add Custom Script and Styles on pages or on whole site using header and footer
 * Version:           1.0.0
 * Author:            Jagdish Sarma
 * Author URI:        https://jagdish.info/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       c2-custom-script-styles
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'C2_CUSTOM_SCRIPT_STYLES_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-c2-custom-script-styles-activator.php
 */
function activate_c2_custom_script_styles() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-c2-custom-script-styles-activator.php';
	C2_Custom_Script_Styles_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-c2-custom-script-styles-deactivator.php
 */
function deactivate_c2_custom_script_styles() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-c2-custom-script-styles-deactivator.php';
	C2_Custom_Script_Styles_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_c2_custom_script_styles' );
register_deactivation_hook( __FILE__, 'deactivate_c2_custom_script_styles' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-c2-custom-script-styles.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_c2_custom_script_styles() {

	$plugin = new C2_Custom_Script_Styles();
	$plugin->run();

}
run_c2_custom_script_styles();
