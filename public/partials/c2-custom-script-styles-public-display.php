<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://jagdish.info/
 * @since      1.0.0
 *
 * @package    C2_Custom_Script_Styles
 * @subpackage C2_Custom_Script_Styles/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
