<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://jagdish.info/
 * @since      1.0.0
 *
 * @package    C2_Custom_Script_Styles
 * @subpackage C2_Custom_Script_Styles/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    C2_Custom_Script_Styles
 * @subpackage C2_Custom_Script_Styles/includes
 * @author     Jagdish Sarma <jagdishsarma.mld@gmail.com>
 */
class C2_Custom_Script_Styles_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'c2-custom-script-styles',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
