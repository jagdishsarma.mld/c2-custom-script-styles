<?php

/**
 * Fired during plugin activation
 *
 * @link       https://jagdish.info/
 * @since      1.0.0
 *
 * @package    C2_Custom_Script_Styles
 * @subpackage C2_Custom_Script_Styles/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    C2_Custom_Script_Styles
 * @subpackage C2_Custom_Script_Styles/includes
 * @author     Jagdish Sarma <jagdishsarma.mld@gmail.com>
 */
class C2_Custom_Script_Styles_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
